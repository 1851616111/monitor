package monitor

import (
	"log"
	"os"
	"strconv"
	"time"
	"net/http"
)

const (
	File_PATH       = "/proc/meminfo"
	ASCII_START     = 58 //:
	ASCII_END       = 10 //\n
	FATAL           = 3
	STATIS_INTERVAL = 5 //second
)

var (
	file   *os.File
	ch     chan *meminfo
	Status bool
	buffer []byte
)

func init() {
	file, _ = os.OpenFile(File_PATH, os.O_RDONLY, 0)
	buffer = make([]byte, 60)
	ch = make(chan *meminfo, 20)
	Status = true
}

func Monitor() {

	log.Println("start monitor")
	go func() {
		calculate()
	}()

	for {
		time.Sleep(time.Second * time.Duration(STATIS_INTERVAL))
		tmp := getMemInfo()
		ch <- getTotal(tmp)
	}

}

func Stat(w http.ResponseWriter)  {
	if !Status {
		http.Error(w, "service busy", 503)
	}
}

func calculate() {
	var f, t int
	for c := range ch {
		f, _ = strconv.Atoi(string(c.Free))
		t, _ = strconv.Atoi(string(c.Total))
		result := f / t
		if result <= FATAL && Status {
			Status = false
		} else if f/t > FATAL && !Status {
			Status = true
		}
		log.Println("memory total: %d, used %d, free %d", t, t-f, f)
	}
}

func getMemInfo() []byte {
	n, _ := file.Read(buffer)
	if n == 0 {
		log.Println("read %s nothing", File_PATH)
	}

	file.Seek(0, 0)

	tmp := []byte{}
	for _, v := range buffer {
		if v != 32 {
			tmp = append(tmp, v)
		}
	}
	return tmp
}

func getTotal(b []byte) *meminfo {
	var total_start, total_end, free_start, free_end, counter int

	for i, v := range b {
		switch v {
		case ASCII_START:
			if counter == 0 {
				total_start = i
			} else {
				free_start = i
			}
		case ASCII_END:
			if counter == 0 {
				total_end = i
			} else {
				free_end = i
			}
			counter++
		}
	}
	return &meminfo{b[total_start+1 : total_end-2], b[free_start+1 : free_end-2]}
}

type meminfo struct {
	Total []byte
	Free  []byte
}
